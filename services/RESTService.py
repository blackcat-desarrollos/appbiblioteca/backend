import pymongo
import logging as log
import functools

from bson import objectid
from classes.Utils import BSONtoJSON, ParsedConfig
from classes.Libro import Libro

MONGO_CONFIG = {
    "mongo_host": ParsedConfig.get('mongodb', 'mongo_host'),
    "mongo_port": ParsedConfig.get('mongodb', 'mongo_port'),
    "bdd_name": ParsedConfig.get('mongodb', 'bdd_name'),
    "collection_name": ParsedConfig.get('mongodb', 'collection_name')
}


class ApiService(object):
    client = pymongo.MongoClient(MONGO_CONFIG['mongo_host'], int(MONGO_CONFIG['mongo_port']),
                                 serverSelectionTimeoutMS=1)
    connBDD = client[ParsedConfig.get('mongodb', 'bdd_name')]
    connLibros = connBDD[MONGO_CONFIG['collection_name']]

    def GuardarLibros(self, docs):
        try:
            if not docs:
                raise Exception("docs error")
            elif not isinstance(docs, list):
                raise Exception("docs not array")
            else:
                docsArr = []
                fail = []
                for b in docs:
                    lib = Libro(body=b)
                    if lib.validSchema['valid']:
                        del lib.validSchema
                        docsArr.append(lib.__dict__)
                    else:
                        fail.append(lib.__dict__)
                return {
                    "inserted_ids": ApiService.connLibros.insert_many(docsArr).inserted_ids,
                    "data": docsArr,
                    "failed": fail
                }
        except Exception as err:
            raise Exception(err)

    def ConseguirLibros(self, values):
        try:
            docArray = []
            qry = ApiService.connLibros.find({})
            for document in qry:
                docArray.append(BSONtoJSON(document))
            print(docArray)
            return {
                "collection": qry.collection.name,
                "data": docArray,
                "quantity": len(docArray)
            }
        except Exception as err:
            raise Exception(err)

    def BorrarLibros(self, oids):
        try:
            if not oids:
                raise Exception('docs bad request')
            elif not isinstance(oids, list):
                raise Exception("docs not array")
            else:
                docsArr = []
                functools.reduce()
                for oid in oids:
                    docsArr.append(objectid.ObjectId(oid))
                delete = ApiService.connLibros.delete_many({"_id": {"$in": docsArr}})
                return {
                    "deleted": True if delete.raw_result['n'] > 0 else False,
                    "raw": delete.raw_result,
                    "deleted_oid": oids
                }
        except Exception as err:
            raise Exception(err)

    def EditarLibro(self, docs):
        try:
            if not docs:
                raise Exception('docs bad request')
            elif not isinstance(docs, list):
                raise Exception("docs not array")
            else:
                resArray = []
                for doc in docs:
                    resArray.append(ApiService.connLibros.find_one_and_update(
                        {"_id": objectid.ObjectId(doc['$oid'])},
                        {"$set": doc['edit']}, upsert=True
                    ))
                return {
                    "resArray": resArray
                }
        except Exception as err:
            raise Exception(err)


def ApiError(e):
    err = str(e)
    log.error("Api Error: " + err)
    return {"Api Error": err}, 400
