import configparser
import json
from bson import json_util

ParsedConfig = configparser.ConfigParser()
ParsedReader = ParsedConfig.read('config/properties.ini')


def BSONtoJSON(bson):
    return json.loads(json_util.dumps(bson))
