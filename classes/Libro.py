import datetime
import jsonschema
from jsonschema import validate


class Libro(object):
    Schema = {
        "$schema": "http://json-schema.org/draft/2019-09/output/schema",
        "description": "Un Libro",
        "type": "object",
        "properties": {
            "Nombre": {"type": "string"},
            "lugarPublicacion": {"type": "string"},
            "Publicador": {"type": "string"},
            "CantEjemplares": {"type": "number"},
            "Autores": {"type": "array"},
            "Lenguaje": {"type": "array"},
            "Etiquetas": {"type": "array"},
            "Tipo": {"type": "array"},
            "anioPublicacion": {"type": "array"},
            "UltimaModificacion": {"type": "object"},
            "LugarPublicacion": {"type": "string"},
        },
        "required": ["Nombre"]
    }

    def __init__(self, **params):
        # Strings
        print("PARAMS", params["body"])
        self.Nombre = params["body"]["Nombre"] if params["body"]["Nombre"] else None
        #self.LugarPublicacion = params["body"]["LugarPublicacion"] if params["body"]["LugarPublicacion"] else None
        #self.Publicador = params["body"]["Publicador"] if params["body"]["Publicador"] else None
        # Numbers
        # self.CantEjemplares = CantEjemplares  
        # Arrays
        #self.Autores = params["body"]["Autores"] if params["body"]["Autores"] else None

        # self.Lenguaje = Lenguaje
        # self.Etiquetas = Etiquetas
        # self.Tipo = Tipo
        # Fechas
        # self.anioPublicacion = datetime.datetime(anioPublicacion)
        # Defaults
        # self.UltimaModificacion = datetime.datetime.now().strftime("%c")
        self.validSchema = self.validateSchema()

    def validateSchema(self):
        try:
            validate(instance=self.__dict__, schema=Libro.Schema)
        except jsonschema.exceptions.ValidationError as err:
            return {"valid": False, "error": err}
        except Exception as err:
            return {"valid": False, "error": err}
        return {"valid": True}
