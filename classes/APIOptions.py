import datetime
import jsonschema
from jsonschema import validate


class Libro(object):
    Schema = {
        "$schema": "http://json-schema.org/draft/2019-09/output/schema",
        "description": "Un Libro",
        "type": "object",
        "properties": {
            "Nombre": {"type": "string"},
            "lugarPublicacion": {"type": "string"},
            "Publicador": {"type": "string"},
            "CantEjemplares": {"type": "number"},
            "Autores": {"type": "array"},
            "Lenguaje": {"type": "array"},
            "Etiquetas": {"type": "array"},
            "Tipo": {"type": "array"},
            "anioPublicacion": {"type": "array"},
            "UltimaModificacion": {"type": "object"},
            "LugarPublicacion": {"type": "string"},
        },
        "required": ["Nombre"]
    }

    def __init__(self, **params):
        # Strings
        print("PARAMS", params["body"])
        self.Nombre = params["body"]["Nombre"] if params["body"]["Nombre"] else None
        self.Autores = params["body"]["Autores"] if params["body"]["Autores"] else None
        self.validSchema = self.validateSchema()

    def validateSchema(self):
        try:
            validate(instance=self.__dict__, schema=Libro.Schema)
        except jsonschema.exceptions.ValidationError as err:
            return {"valid": False, "error": err}
        except Exception as err:
            return {"valid": False, "error": err}
        return {"valid": True}
