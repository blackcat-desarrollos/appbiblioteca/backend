import logging
from flask import Flask, request, make_response
from flask_cors import CORS, cross_origin
from services.RESTService import *
from classes.Utils import *

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
RESTApi = ApiService()
logging.basicConfig(level=logging.INFO)

AcceptedMethods = ['GET', 'POST', 'DELETE', 'PUT']


@app.route("/rest/biblioteca", methods=AcceptedMethods)
@cross_origin()
def REST():
    try:
        if request.method not in AcceptedMethods:
            raise Exception("Invalid Method")
        incomingJson, Options = readRequestJson(request.get_json())
        print("incomingJson: {} Options: {} \n".format(incomingJson, Options))
        if request.method == 'GET':
            res = RESTApi.ConseguirLibros(incomingJson)
        elif request.method == 'POST':
            res = BSONtoJSON(RESTApi.GuardarLibros(incomingJson))
        elif request.method == 'DELETE':
            res = RESTApi.BorrarLibros(incomingJson)
        elif request.method == 'PUT':
            res = BSONtoJSON(RESTApi.EditarLibro(incomingJson))
        return make_response(res)
    except Exception as err:
        return make_response(ApiError(err))


def readRequestJson(js=None, docs=None, options=None):
    if js is None or js == {}:
        return docs, options
    for k in js:
        if k == "docs":
            docs = js[k]
        elif k == "options":
            options = js[k]
    return docs, options


if __name__ == '__main__':
    configurations = ""
    for sect in ParsedConfig.sections():
        configurations = configurations + 'Section: {}'.format(sect)
        for k, v in ParsedConfig.items(sect):
            configurations = configurations + ' {} = {} \n'.format(k, v)

    log.log(configurations)
    app.run(port=ParsedConfig.get('API', 'api_port'))
