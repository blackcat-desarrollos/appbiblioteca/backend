# Example API Calls
Created viernes 25 Diciembre 2020

**URL**: <http://127.0.0.1:5000/rest/biblioteca>

[API Calls](#API Calls)

[+DELETE](#API Calls:DELETE)
[+POST](#API Calls:POST)
[+GET](#API Calls:GET)
[+PUT](#API Calls:PUT)




# DELETE
Created viernes 25 Diciembre 2020

**REQUEST**
{
"docs": ["5fe62354cdd5740f37b7f08a"]
}

**RESPONSE**
{
"deleted": true,
"docs": [
"5fe62354cdd5740f37b7f08a"
],
"raw": {
"n": 1,
"ok": 1.0
}
}

# GET
Created viernes 25 Diciembre 2020

**REQUEST**
{}

**RESPONSE**
{
"collection": "Libros",
"docs": [
{
"Autores": [
"Renzo"
],
"Nombre": "Un libro",
"_id": {
"$oid": "5fe1da93d8dd24418cd715bc"
},
"validSchema": {
"valid": true
}
}
],
"quantity": 8
}

# POST
Created viernes 25 Diciembre 2020

**REQUEST**
{
"docs": [
{
"Autores": [
"Renzo"
],
"Nombre": "El libro grupal"
},
{
"Autores": [
"Not Renzo"
],
"Nombre": "Libro grupal Tomo 2"
}
]
}

**RESPONSE**
{
"docs": [
{
"Autores": [
"Not Renzo"
],
"Nombre": "Libro grupal Tomo 2",
"_id": {
"$oid": "5fe624b64d6eca0bb6b5ba60"
}
}
],
"failed": [],
"inserted_ids": [
{
"$oid": "5fe624b64d6eca0bb6b5ba60"
}
]
}

# PUT
Created viernes 25 Diciembre 2020

**REQUEST**
{
"docs": [
{
"$oid": "5fe624b64d6eca0bb6b5ba60",
"edit": {
"Nombre": "Libro grupal Tomo 3"
}
}
]
}

**RESPONSE**
{
"resArray": [
{
"Autores": [
"Not Renzo"
],
"Nombre": "Libro grupal Tomo 33232",
"_id": {
"$oid": "5fe624b64d6eca0bb6b5ba60"
}
}
]
}

# Docs
Created viernes 25 Diciembre 2020


