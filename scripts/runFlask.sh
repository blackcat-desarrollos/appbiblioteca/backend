#!/usr/bin/env sh
export FLASK_ENV=development
export FLASK_DEBUG=1
export FLASK_APP="$(pwd)/API.py"
echo "Running $FLASK_APP..."
mongod > "$(pwd)/mongo.log" &
py -m flask run --reload
